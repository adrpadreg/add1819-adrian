#A1: Clientes ligeros con LTSP

Usaremos 2 MVs para montar clientes ligeros con LTSP

#Servidor LTSP

##Preparar la MV Server

Crear la MV del servidor con dos interfaces de red.

* La primera interfaz externa, para comunicarse con Internet. Configurarla en VBox como adaptador puente
* La segunda interna, para conectarse con los clientes ligeros. Configurarla en VBox como "red interna". La IP de esta interfaz de red debe ser estática y debe estar en la misma red que los clientes

![interfaces-server](./images/interfaces-server.png)

##Instalación del SSOO

* Instalar un SO GNU/Linux Xubuntu en la MV del servidor 

Una vez se instaló el sistema operativo, tuve que hacer la configuración de red de la MV. Para ello vamos al fichero `/etc/network/interfaces`:

![network](./images/network.png)

> enp0s3 es nuestra interfaz externa, mientras que enp0s8 es la que usaremos para conectarnos con los clientes ligeros.

* Había que incluir la salida de los comandos siguientes:

![comandos](./images/comandos.png)

![comandos2](./images/comandos2.png)

![comandos03](./images/comandos03.png)

* Teníamos que crear 3 usuarios locales:

![users](./images/users.png)

##Instalar el servicio LTSP

* Instalar el servidor SSH para permitir acceso remoto a la máquina:

![ssh](./images/ssh.png)

* Instalar servidor de clientes ligeros para Xubuntu. Con `apt-get install ltsp-server-standalone`:

![standalone](./images/standalone.png)

* Ahora vamos a crear una imagen del SO a partir del sistema real haciendo `ltsp-build-client`. La imagen del SO se cargará en la memoria de los clientes ligeros

![build-client](./images/build-client.png)

* Ejecutar `ltsp-info`, para consultar información:

![ltsp-info](./images/ltsp-info.png)

Revisamos la configuración del servicio DHCP instalado junto con LTSP. Cambié las opciones de arranque del servicio DHCP, estableciendo la variable INTERFACES con el nombre de la interfaz de red donde debe trabajar, en mi caso enp0s8. Esto lo hice en el fichero `etc/default/isc-dhcp-server`:

![default](./images/default.png)

También cambié las opciones de arranque del servicio TFTP editando el fichero `/etc/default/tftpd-hpa`. Establecí la variable TFTP_ADDRESS al valor de la IP:PORT de la interfaz de trabajo, en mi caso 192.168.67.1:69

![tftpd](./images/tftpd.png)

Reinicié los dos servicios para que se guardaran los cambios e hice un "status" para comprobar que funcionaban correctamente:

![dhcp](./images/dhcp.png)

![status-tftpd](./images/status-tftpd.png)

#Preparar MV Cliente

Crear la MV cliente en VirtualBox:

* Sin disco duro y sin unidad de DVD
* Sólo tiene RAM, floppy
* Tarjeta de red PXE en modo "red interna"

Con el servidor encendido, iniciar la MV cliente desde red/PXE:

* Comprobar que todo funciona correctamente

Yo tuve problemas en esta parte, ya que cuando fui a acceder con la máquina cliente me estaba iniciando hasta que me apareció la siguiente pantalla dividida en dos con tonos rosados:

![client](./images/client.png)


* URL del video de la práctica en funcionamiento: [Clientes ligeros LTSP](https://www.youtube.com/watch?v=fTy7gKJwemQ)
