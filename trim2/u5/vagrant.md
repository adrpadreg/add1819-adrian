#Vagrant

#Primeros pasos

##Instalar

La instalación debemos hacerla en una máquina real (`apt-get install vagrant`). En mi caso ya estaba instalado.

##Proyecto

* Crear un directorio para nuestro proyecto vagrant:

![01](./images-A1/01.png)

##Imagen, caja o box

* Ahora necesitamos obtener una imagen (caja, box) de un sistema operativo. Vamos, por ejemplo, a conseguir una imagen de un `Ubuntu Precise de 32 bits`:

![02](./images-A1/02.png)

* Para usar una caja determinada en nuestro proyecto, modificamos el fichero `Vagrantfile`. Cambiamos la línea `config.vm.box = "base"` por `config.vm.box = "micaja08_ubuntu_precise32"`

![03](./images-A1/03.png)

##Iniciar una nueva máquina

Vamos a iniciar una máquina virtual nueva usando Vagrant:

* `cd mivagrant08`

* `vagrant up`: comando para iniciar una nueva instancia de la máquina

![04](./images-A1/04.png)

* `vagrant ssh`: Conectar/entrar en nuestra máquina virtual usando SSH

![05](./images-A1/05.png)

* Otros comandos útiles de Vagrant son:

-`vagrant suspend`

-`vagrant resume`

-`vagrant halt`

-`vagrant status`

-`vagrant destroy`

#Configuración del entorno virtual

##Carpetas sincronizadas

* Para identificar las carpetas compartidas dentro del entorno virtual, hacemos:

![06](./images-A1/06.png)

##Redireccionamiento de los puertos

Cuando trabajamos con MVs, es frecuente usarlas para proyectos enfocados a la web, y para acceder a las páginas es necesario configurar el enrutamiento de puertos.

* Entramos en la MV e instalamos apache.

-`vagrant ssh`
-`apt-get install apache2`

![10](./images-A1/10.png)

* Modificar el fichero `Vagrantfile`, de modo que el puerto 4567 del sistema anfitrión sea enrutado al puerto 80 del ambiente virtualizado:

![07](./images-A1/07.png)

* Luego iniciamos la MV

Para confirmar que hay un servicio a la escucha en 4567, desde la máquina real podemos ejecutar los siguientes comandos:

* `nmap -p 4500-4600 localhost`

* `netstat -ntap`

![08](./images-A1/08.png)

![09](./images-A1/09.png)

* En la máquina real, abrimos el navegador web con el URL `http://127.0.0.1:4567`. En realidad estamos accediendo al puerto 80 de nuestro sistema virtualizado.

![11](./images-A1/11.png)

#Suministro

Uno de los mejores aspectos de Vagrant es el uso de herramientas de suministro. Esto es, ejecutar "una receta" o una serie de scripts durante el proceso de arranque del entorno virtual para instalar, configurar y personalizar un sin fin de aspectos del SO del sistema anfitrión.

* `vagrant halt`, apagamos la MV

* `vagrant destroy` y la destruimos para volver a empezar

![12](./images-A1/12.png)

##Suministro mediante shell script

Ahora vamos a suministrar a la MV un pequeño script para instalar Apache

* Crear el script `install_apache.sh`, dentro del proyecto:

![13](./images-A1/13.png)

> Poner permisos de ejecución al script.

Vamos a indicar a Vagrant que debe ejecutar dentro del entorno virtual un archivo `install_apache.sh`

* Modificar Vagrantfile y agregar la siguiente línea a la configuración: `config.vm.provision :shell, :path => "install_apache.sh"`

![14](./images-A1/14.png)

* Volvemos a crear la MV

Podremos notar, al iniciar la máquina, que en los mensajes de salida muestran mensajes que indican cómo se va instalando el paquete de Apache que indicamos.

![15](./images-A1/15.png)

* Para verificar que efectivamente el servidor Apache ha sido instalado e iniciado, abrimos navegador en la máquina real con URL `http://127.0.0.1:4567`

![16](./images-A1/16.png)

##Suministro mediante Puppet

* Modificar el archivo Vagrantfile de la siguiente forma:

![17](./images-A1/17.png)

* Crear un fichero `manifests/software.pp`, con las órdenes/instrucciones puppet para instalar el programa `nmap`.

![18](./images-A1/18.png)

Para que se apliquen los cambios de configuración, tenemos dos formas:

* Yo elegí la opción A, parar la MV, destruirla y crearla de nuevo (`vagrant halt`, `vagrant destroy` y `vagrant up`)

![19](./images-A1/19.png)

![20](./images-A1/20.png)

![21](./images-A1/21.png)

#Nuestra caja personalizada 

En los apartados anteriores hemos descargado una caja/box de un repositorio de Internet, y luego la hemos provisionado para personalizarla. En este apartado vamos a crear nuestra propia caja/box personalizada a partir de una MV de VirtualBox.

##Preparar la MV VirtualBox

Lo primero que tenemos que hacer es preparar nuestra máquina virtual con una configuración por defecto, por si queremos publicar nuestro Box, ésto se realiza para seguir un estándar y que todo el mundo pueda usar dicho Box.

* Crear una MV VirtualBox nueva o usar una que ya tengamos.

* Instalar OpenSSH Server

* Crear el usuario Vagrant, para poder acceder a la máquina virtual por SSH. A este usuario le agregamos una clave pública para autorizar el acceso sin clave desde Vagrant.

![22](./images-A1/22.png)

![23](./images-A1/23.png)

![24](./images-A1/24.png)

![25](./images-A1/25.png)

* Poner clave `vagrant` al usuario vagrant y al usuario root

Tenemos que conceder permisos al usuario vagrant para que pueda configurar la red, instalar software, montar carpetas compartidas, etc. para ello debemos configurar `/etc/sudoers` (visudo) para que no nos solicite la password de root, cuando realicemos estas operaciones con el usuario vagrant

* Añadir `vagrant ALL=(ALL) NOPASSWD: ALL` a /etc/sudoers

![26](./images-A1/26.png)

> Hay que comprobar que no existe una linea indicando requiretty si existe la comentamos

* Debemos asegurarnos que tenemos instalado las VirtualBox Guest Additions, para conseguir mejoras en el S.O o poder compartir carpetas con el anfitrión.

##Crear la caja vagrant

Una vez hemos preparado la MV ya podemos crear el box

* Vamos a crear una nueva carpeta `mivagrant08conmicaja`, para este nuevo proyecto vagrant

* Ejecutamos `vagrant init` para crear el fichero de configuración nuevo.

* Localizar el nombre de nuestra máquina VirtualBox

* Crear la caja `package.box` a partir de la MV. Comprobamos que se ha creado la caja `package.box` en el directorio donde hemos ejecutado el comando

![27](./images-A1/27.png)

* Muestro la lista de cajas disponibles, pero sólo tengo 1 porque todavía no he incluido la que acabo de crear. Finalmente, añado la nueva caja creada por mí al repositorio de vagrant.

![28](./images-A1/28.png)

* Levanté la máquina y me dió error, por tanto, hice la siguiente configuración en Vagrantfile:

`config.ssh.username = 'root'`
`config.ssh.password = 'vagrant'`
`config.ssh.insert_key = 'true'`

Aún así, no me permitía levantarla ya que llegaba a una parte en la que me pedía una clave, que introduje, pero no surtió efecto:

![29](./images-A1/29.png)

* Haciendo `vagrant ssh` nos conectamos sin problemas con la máquina


























