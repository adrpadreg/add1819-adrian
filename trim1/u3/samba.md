#Recursos compartidos SMB/CIFS (OpenSUSE)

#Samba

Samba con OpenSUSE 13.2 y Windows 7

* Vamos a necesitar las siguientes 3 MVs:

1. MV1: Un servidor GNU/Linux OpenSUSE 13.2 con IP estática (172.18.8.31)
1. MV2: Un cliente GNU/Linux OpenSUSE 13.2 con IP estática (172.18.8.32)
1. MV3: Un cliente Windows 7 con IP estática (172.18.8.11)

##Servidor Samba (MV1)

* Preparativos

-Configurar el servidor GNU/Linux. Nombre de equipo: smb-server08

-Añadir en `/etc/hosts` los equipos `smb-cli08a` y `smb-cli08b`

Captura de los comandos en el servidor:

![01-comandos](./images/01-comandos.png)

* Usuarios locales

Vamos a GNU/Linux, y creamos los siguientes grupos y usuarios:

-Crear los grupos `jedis`, `siths` y `starwars`

-Crear el usuario `smbguest`. Para asegurarnos que nadie puede usar `smbguest` para entrar en nuestra máquina mediante login, vamos a modificar este usuario y le ponemos como shell `/bin/false`

![01-1](./images/01-1.png)

-Dentro del grupo `jedis` incluir a los usuarios `jedi1`, `jedi2` y `supersamba`

-Dentro del grupo `siths` incluir a los usuarios `sith1`, `sith2` y `supersamba`

-Dentro del grupo `starwars`, poner a todos los usuarios `siths`, `jedis`, `supersamba` y a `smbguest`

Usuarios:

![01-users](./images/01-users.png)

Grupos:

![01-groups](./images/01-groups.png)

* Crear las carpetas para los futuros recursos compartidos

Vamos a crear las carpetas de los recuros compartidos con los permisos siguientes:

1. `/srv/samba08/public.d`

-Usuario propietario `supersamba`

-Grupo propietario `starwars`

-Poner permisos 775

![01-3](./images/01-3.png)

2. `/srv/samba08/corusant.d`

-Usuario propietario `supersamba`

-Grupo propietario `siths`

-Poner permisos 770

![01-4](./images/01-4.png)

3. `/srv/samba08/tatooine.d`

-Usuario propietario `supersamba`

-Grupo propietario `jedis`

-Poner permisos 770

![01-5](./images/01-5.png)

* Instalar Samba Server

-Vamos a hacer una copia de seguridad del fichero de configuración existente:

![01-6](./images/01-6.png)

-Procedemos a instalar el servicio de Samba:

![01-2](./images/01-2.png)

-Yast -> Samba Server

Workgroup: `starwars`

Sin controlador de dominio

-En la pestaña de Inicio definimos

Iniciar el servicio durante el arranque de la máquina 

Ajustes del cortafuegos -> Abrir puertos

![01-7](./images/01-7.png)

* Configurar el servidor Samba

Vamos a configurar los recursos compartidos del servidor Samba. Para ello, modifiqué el fichero `/etc/samba/smb.conf`, dejándolo así:

![01-8](./images/01-8.png)

> Comando cat /etc/samba/smb.conf

-Abrir el terminal para comprobar los resultados:

![01-9](./images/01-9.png)

> Comando testparm

* Usuarios Samba

Después de crear los usuarios en el sistema, hay que añadirlos a Samba

-`smbpasswd -a nombreusuario`, para crear clave de Samba para un usuario del sistema

-`pdbedit -L`, para comprobar la lista de usuarios Samba

![01-10](./images/01-10.png)

* Reiniciar 

Ahora que hemos terminado con el servidor, hay que reiniciar el servicio para que se lean los cambios de configuración

![01-11](./images/01-11.png)

![01-12](./images/01-12.png)

> No conseguí arrancar el servicio nmb. Se miró con el profesor en clase, pero no se encontró una solución y al final lo dejé así

Captura de comandos de comprobación:

![01-13](./images/01-13.png)

![01-14](./images/01-14.png)

##Windows (MV3 smb-cli08b)

-Configurar el cliente Windows

-Usar nombre `smb-cli08b` y la IP que hemos establecido

-Configurar el fichero `.../etc/hosts` de Windows

* Cliente Windows GUI

Desde un cliente Windows vamos a acceder a los recursos compartidos del servidor Samba

-Comprobar los accesos de todas las formas posibles. Como si fuéramos un sith, un jedi y/o un invitado.

![02-1](./images/02-1.png)

![02-2](./images/02-2.png)

Captura de los comandos para comprobar los resultados:

![02-3](./images/02-3.png)

![02-4](./images/02-4.png)

![02-5](./images/02-5.png)

* Cliente Windows comandos

-En el cliente Windows para consultar todas las conexiones/recursos conectados hacemos `net use`

![02-6](./images/02-6.png)

> Si hubiera alguna conexión abierta la cerramos con el comando "net use * /d /y"

Captura de los siguientes comandos: 

-`net use /?`

![02-7](./images/02-7.png)

-Vamos a conectarnos desde la máquina Windows al servidor Samba con el comando `net use A: \\172.18.8.31\tatooine /USER:jedi1`

![02-8](./images/02-8.png)

-Con el comando `net view`, vemos las máquinas (con recursos CIFS) accesibles por la red

![02-10](./images/02-10.png)

> Me salió Error de sistema 6118, La lista de servidores de este grupo de trabajo no se encuentra disponible en este momento

![02-9](./images/02-9.png)

* Montaje automático

-El comando `net use S: \\172.18.8.31\corusant /USER:sith1` establece una conexión del recurso corusant y lo monta en la unidad S

![02-11](./images/02-11.png)

-Ahora podemos entrar en la unidad S ("s:") y crear carpetas, etc.

![02-12](./images/02-12.png)

Captura de comandos para comprobar resultados:

![02-13](./images/02-13.png)

![02-14](./images/02-14.png)

![02-15](./images/02-15.png)

##Cliente GNU/Linux (MV2 smb-cli08a)

-Configurar el cliente GNU/Linux

-Usar nombre `smb-cli08a` y la IP que hemos establecido

-Configura el fichero `/etc/hosts` de la máquina

Desde el entorno gráfico, podemos comprobar el acceso a recursos compartidos SMB/CIFS. Escribimos smb://172.18.8.31:

![3-01](./images/3-01.png)

-Probar a crear carpetas/archivos en `corusant` y en `tatooine`

Corusant:

![3-02](./images/3-02.png)

![3-03](./images/3-03.png)

Tatooine:

![3-04](./images/3-04.png)

![3-05](./images/3-05.png)

-Comprobar que el recurso `public` es de sólo lectura

![3-06](./images/3-06.png)

-Captura de los comandos para comprobar resultados:

![3-07](./images/3-07.png)

![3-08](./images/3-08.png)

![3-09](./images/3-09.png)
![3-10](./images/3-10.png)

* Cliente GNU/Linux comandos

-Vamos a un equipo GNU/Linux que será nuestro cliente Samba. Desde este equipo usaremos comandos para acceder a la carpeta compartida

-Comprobar el uso de las siguientes herramientas:

![3-11](./images/3-11.png)

-Ahora crearemos en local la carpeta `/mnt/samba08-remoto/corusant`

-MONTAJE: Con el usuario root, usamos el siguiente comando para montar un recurso compartido de Samba Server, como si fuera una carpeta más de nuestro sistema: `mount -t cifs //172.18.8.31/corusant /mnt/samba08-remoto/corusant -o username=sith1`

![3-12](./images/3-12.png)

-COMPROBAR: Ejecutar el comando `df -hT`. Veremos que el recurso ha sido montado:

![3-13](./images/3-13.png)

-Captura de comandos para comprobar resultados:

![3-14](./images/3-14.png)

![3-15](./images/3-15.png)

![3-16](./images/3-16.png)

* Montaje automático 

Acabamos de acceder a los recursos remotos, realizando un montaje de forma manual. Si reiniciamos el equipo cliente, podremos ver que los montajes realizados de forma manual ya no están. Si queremos volver a acceder a los recursos remotos debemos repetir el proceso de montaje manual, a no ser que hagamos una configuración de montaje permanente o automática.

-Para configurar acciones de montaje automáticos cada vez que se inicie el equipo, debemos configurar el fichero `/etc/fstab` de la siguiente manera, por ejemplo:

![3-17](./images/3-17.png)

-Reiniciar el equipo y comprobar que se realiza el montaje automático al inicio:

![3-18](./images/3-18.png)

##Preguntas para resolver

* ¿Las claves de los usuarios en GNU/Linux deben ser las mismas que las que usa Samba?

No, no tienen por que ser las mismas

* ¿Puedo definir un usuario en Samba llamado sith3, y que no exista como usuario del sistema?

No, no es posible, tiene que existir en el sistema.

* ¿Cómo podemos hacer que los usuarios sith1 y sith2 no puedan acceder al sistema pero sí al samba? (Consultar `/etc/passwd`)

Lo que hay que hacer es ponerles como shell `/bin/false`, como en el caso de `smbguest`:

![4-01](./images/4-01.png)

* Añadir el recurso [homes] al fichero `smb.conf` según los apuntes. ¿Qué efecto tiene?

![4-02](./images/4-02.png)

En ésta sección se configuran los parámetros para compartir la carpeta home (carpeta donde se almacena el perfil y todos los documentos) de cada usuario. Esta sección es opcional. Si no existe, no se compartirán las carpetas home de cada usuario. Se utiliza cuando se desean crear perfiles móviles de forma que cuando se identifique el usuario en cualquiera de los PCs de la red, se mapee de forma automática su perfil.






























