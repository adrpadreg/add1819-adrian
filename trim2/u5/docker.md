#Docker

#Requisitos

Vamos a usar MV OpenSUSE. Nos aseguraremos que tiene una versión del Kernel 3.10 o superior:

![01](./images-A2/01.png)

#Instalación y primeras pruebas

* Ejecutar como superusuario:

![02](./images-A2/02.png)

![03](./images-A2/03.png)

* Salimos de la sesión y volvemos a entrar con nuestro usuario

* Ejecutamos lo siguiente con nuestro usuario para comprobar que todo funciona:

![04](./images-A2/04.png)

> Muestra las imágenes descargadas hasta ahora y los contenedores creados

![05](./images-A2/05.png)

> Descarga y ejecuta un contenedor con la imagen hello-world

![06](./images-A2/06.png)

#Configuración de la red

Habilitar el acceso a la red externa a los contenedores

Si queremos que nuestro contenedor tenga acceso a la red exterior, debemos activar la opción IP_FORWARD (`net.ipv4.ip_forward`). Lo podemos hacer en YAST.

¿Recuerdas lo que implica `forwarding` en los dispositivos de red?

* Para openSUSE Leap 42.2 (cuando el método de configuración de red es Wicked). `Yast -> Dispositivos de red -> Encaminamiento -> Habilitar reenvío IPv4`

Reiniciar el equipo para que se apliquen los cambios.

#Crear un contenedor manualmente

Nuestro SO base es OpenSUSE, pero vamos a crear un contenedor Debian8, y dentro instalaremos Nginx

##Crear una imagen

Vemos las imágenes disponibles localmente

![07](./images-A2/07.png)

Buscamos en los repositorios de Docker Hub contenedores con la etiqueta "debian":

![08](./images-A2/08.png)

Descargamos contenedor "debian:8" en local

![09](./images-A2/09.png)

* Vamos a crear un contenedor con nombre `mv_debian` a partir de la imagen `debian:8`, y ejecutaremos `/bin/bash`:

![10](./images-A2/10.png)

> Con `cat /etc/motd` comprobamos que estamos en Debian

![11](./images-A2/11.png)

![12](./images-A2/12.png)

![13](./images-A2/13.png)

![14](./images-A2/14.png)

> Con `/usr/sbin/nginx` iniciamos el servicio nginx

* Creamos un fichero HTML (`holamundo.html`)

![15](./images-A2/15.png)

* Creamos también un script `/root/server.sh` con el siguiente contenido:

![16](./images-A2/16.png)

> Poner permisos de ejecución al script!

> Este script inicia el programa/servicio y entra en un bucle, para permanecer activo y que no se cierre el contenedor. Más adelante cambiaremos este script por la herramienta `supervisor`

* Ya tenemos nuestro contenedor auto-suficiente de Nginx, ahora debemos crear una nueva imagen con los cambios que hemos hecho, para esto abrimos otra ventana de terminal y busquemos el IDContenedor:

![17](./images-A2/17.png)

* Ahora con esto podemos crear la nueva imagen a partir de los cambios que realizamos sobre la imagen base:

![18](./images-A2/18.png)

> Los estándares de Docker estipulan que los nombres de las imagenes deben seguir el formato `nombreusuario/nombreimagen`. Todo cambio que se haga en la imagen y no se haga commit se perderá en cuanto se cierre el contenedor.

![19](./images-A2/19.png)

##Crear contenedor 

* Iniciemos el contenedor de la siguiente manera:

![20](./images-A2/20.png)

Los mensajes muestran que el script server.sh está en ejecución. No parar el programa. Es correcto.

> El argumento `-p 80` le indica a Docker que debe mapear el puerto especificado del contenedor, en nuestro caso el puerto 80 es el puerto por defecto sobre el cual se levanta Nginx

> El script `server.sh` nos sirve para iniciar el servicio y permanecer en espera. Lo podemos hacer también con el programa `Supervisor`

* Abrimos una nueva terminal

* `docker ps`, nos muestra los contenedores en ejecución. Podemos apreciar que la última columna nos indica que el puerto 80 del contenedor está redireccionado a un puerto local `0.0.0.0.:32769->80/tcp`

![21](./images-A2/21.png)

* Abrir navegador web y poner URL `0.0.0.0.:32769`. De esta forma nos conectaremos con el servidor Nginx que se está ejecutando dentro del contenedor

![22](./images-A2/22.png)

* Paramos el contenedor y lo eliminamos

![23](./images-A2/23.png)

> Como ya tenemos una imagen docker, podemos crear nuevos contenedores cuando lo necesitemos.

##Más comandos

* `docker start CONTAINERID`, inicia un contenedor que estaba parado

* `docker attach CONTAINERID`, conecta el terminal actual con el interior de contenedor 

#Crear un contenedor con Dockerfile

Ahora vamos a conseguir el mismo resultado del apartado anterior, pero usando un ficheor de configuración, llamado `Dockerfile`

##Comprobaciones iniciales

![24](./images-A2/24.png)

##Preparar ficheros

* Crear directorio `/home/adrian/docker08`, poner dentro los siguientes ficheros.

* Crear el fichero `Dockerfile` con el siguiente contenido:

![25](./images-A2/25.png)

> Los ficheros `server.sh` y `holamundo.html` que vimos en el apartado anterior, tienen que estar en el mismo directorio del fichero `Dockerfile`

##Crear imagen 

El fichero `Dockerfile` contiene la información necesaria para construir el contenedor, veamos:

![26](./images-A2/26.png)

> Aquí construimos la imagen a partir del `Dockerfile`

Debe aparecer nuestra nueva imagen

![27](./images-A2/27.png)

##Crear contenedor y comprobar

* A continuación vamos a crear un contenedor con el nombre `mv_nginx2`, a partir de la imagen `adrian/nginx`, y queremos que este contenedor ejecute el programa `/root/server.sh`

* Desde otra terminal hacer `docker ps` para averiguar el puerto de escucha del servidor Nginx

![28](./images-A2/28.png)

* Comprobar en el navegador URL: `http://localhost:32768`

![29](./images-A2/29.png)

* Comprobar en el navegador URL: `http://localhost:32768/holamundo.html`

![30](./images-A2/30.png)

#Limpiar

Cuando terminamos con los contenedores y ya no los necesitamos, es buena idea pararlos y/o destruirlos.

![31](./images-A2/31.png)























