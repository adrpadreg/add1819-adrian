#!/usr/bin/ruby

if ARGV.size != 1 then
	puts "Usar el programa <delete-users> con 1 argumento"
	puts " filename : Nombre de fichero"
	exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines = content.split("\n")

lines.each do |i|
	puts "Eliminando a: #{i}"
	system("sudo userdel #{i}")		
end
