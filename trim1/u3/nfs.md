#Recursos compartidos NFS (Network File System)

NFS es un protocolo para compartir recursos en red entre sistemas heterogéneos.

* Configuraremos nuestras MVs.

* Configurar acceso remoto para que el profesor pueda acceder a nuestras MVs mientras éstas están encendidas.

* Configurar el firewall cuando sea necesario.

#SO Windows

Para esta parte vamos a necesitar las siguientes máquinas:

* MV Servidor NFS (Windows 2012 Server)

* MV Cliente NFS (Windows 7 Enterprise)

##Servidor NFS Windows

* Instalación del servicio NFS

Vamos a la MV con Windows 2012 Server

-Agregar rol "Servidor de Archivos"

-Marcar "Servicio para NFS"

![01](./images/01.png)

Le damos a `Siguiente >`:

![02](./images/02.png)

Le damos a `Siguiente >`:

![03](./images/03.png)

Hacemos click en `Cerrar`

* Configurar el servidor NFS

-Crear la carpeta `C:\export08\public`. Picar en la carpeta `botón derecho propiedades -> Uso compartido de NFS`, y configurarla para que sea accesible desde la red en modo lectura/escritura con NFS.

![04](./images/04.png)

Hacemos click en `Administrar uso compartido de NFS...`:

![05](./images/05.png)

-Crear la carpeta `C:\export08\private`. Picar en la carpeta `botón derecho propiedades -> Uso compartido de NFS`, y configurarla para que sea accesible desde la red sólo en modo lectura.

![06](./images/06.png)

-Ejecutamos el comando `showmount -e 172.18.8.21`, para comprobar los recursos exportados:

![07](./images/07.png)

##Cliente NFS

Algunas versiones de Windows permiten trabajar con directorios de red NFS nativos de sistemas UNIX. En esta sección veremos como montar y desmontar estos directorios bajo un entorno de Windows 7 Enterprise (Las versiones home y starter no tienen soporte para NFS)

* Instalar el soporte cliente NFS najo Windows

-En primer lugar vamos a instalar el componente cliente NFS para Windows. Para ello vamos a `Panel de Control -> Programas -> Activar o desactivar las características de Windows` y marcamos la casilla `Servicios para NFS` y, dentro de ésta, marcamos `Cliente para NFS`:

![client01](./images/client01.png)

Ahora, vamos a iniciar el servicio cliente NFS, para ello, abrimos una consola con permisos de Administrador y ejecutamos el comando `nfsadmin client start`:

![client02](./images/client02.png)

* Montando el recurso

Ahora necesitamos montar el recurso remoto para poder trabajar con él (hacer con nuestro usuario normal)

-Consultar desde el cliente los recursos que ofrece el servidor, con el comando `showmount -e 172.18.8.21`, y luego, montar el recurso remoto:

![client03](./images/client03.png)

-Hemos decidido asignar la letra de unidad de forma automática, así que si no hay otras unidades de red en el sistema nos asignará la Z

![client04](./images/client04.png)

-Comprobar en el cliente los recursos montados con `net use` y `showmount -a 172.18.8.21`:

![client05](./images/client05.png)

![client06](./images/client06.png)

-`netstat -n`, para comprobar el acceso a los recursos NFS desde el cliente:

![client07](./images/client07.png)

-Para desmontar la unidad simplemente escribimos en una consola: `umount z:`

![client08](./images/client08.png)

-En el servidor ejecutamos el comando `showmount -e 172.18.8.21`, para comprobar los recursos compartidos:

![client09](./images/client09.png)

#SO OpenSUSE

Vamos a necesitar las siguientes máquinas:

* MV Servidor NFS (SO OpenSUSE 13.2): como nombre de esta máquina usar `nfs-server-08`

* MV Cliente NFS (SO OpenSUSE 13.2): como nombre de esta máquina usar `nfs-client-08`

##Servidor NFS 

* Lo primero es instalar el servidor NFS gracias al comando `sudo zypper in nfs-kernel-server`:

![server01](./images/server01.png)

Habilitamos e iniciamos los servicios, y luego comprobamos que están activos:

![server02](./images/server02.png)

![server03](./images/server03.png)

* A continuación, vamos a crear las siguientes carpetas/permisos:

-`/srv/export08/public`, usuario y grupo propietario `nobody:nogroup`

-`/srv/export08/private`, usuario y grupo propietario `nobody:nogroup`, permisos 770

![server04](./images/server04.png)

* Vamos a configurar el servidor NFS de la siguiente forma:

-La carpeta `srv/export08/public`, será accesible desde toda la red en modo lectura/escritura

-La carpeta `srv/export08/private`, sea accesible sólo desde la IP del cliente, sólo en modo lectura

Para ello modificaremos el fichero `/etc/exports` y lo dejaremos de la siguiente manera:

![server05](./images/server05.png)

* Para iniciar y parar el servicio NFS, usaremos Yast o `systemctl`:

![server06](./images/server06.png)

* Para comprobarlo, `showmount -e localhost`. Muestra la lista de recursos exportados por el servidor NFS

![server07](./images/server07.png)

##Cliente NFS

Ahora vamos a comprobar que las carpetas del servidor son accesibles desde el cliente. Procedemos a instalar el software cliente NFS:

![OS01](./images/OS01.png)

* Comprobar conectividad desde cliente al servidor

-`ping 172.18.8.31`: Comprobar la conectividad del cliente con el servidor

![OS02](./images/OS02.png)

-`nmap 172.18.8.31 -Pn`: nmap sirve para escanear equipos remotos, y averiguar que servicios están ofreciendo al exterior. Hay que instalar el paquete nmap, porque normalmente no viene preinstalado.

![OS04](./images/OS04.png)

-`showmount -e 172.18.8.31`: Muestra la lista de recursos exportados por el servidor NFS

![OS05](./images/OS05.png)

* Montar y usar cada recurso compartido desde el cliente

-Crear las carpetas `/mnt/remoto08/public` y `/mnt/remoto08/private`

-`showmount -e 172.18.8.31`, para consultar los recursos que exporta el servidor

![OS06](./images/OS06.png)

-Montar cada recurso compartido en su directorio local correspondiente

![OS07](./images/OS07.png)

-`df -hT`, para comprobar los recursos montados en nuestras carpetas locales

![OS08](./images/OS08.png)

-`netstat -ntap`, para comprobar el acceso a los recursos NFS desde el cliente

![OS09](./images/OS09.png)

-Ahora vamos a crear ficheros/carpetas dentro del recurso public

![OS10](./images/OS10.png)

-Comprobar que el recurso private es de sólo lectura

![OS11](./images/OS11.png)

##Montaje automático

* Configurar montaje automático del recurso compartido public

Yo lo hice con `Yast -> particionador -> NFS -> Añadir`

![OS13](./images/OS13.png)

![OS14](./images/OS14.png)

* Captura con el contenido del fichero `/etc/fstab`:

![OS15](./images/OS15.png)

* Reiniciar el equipo y comprobar que se monta el recurso remoto automáticamente:

![OS16](./images/OS16.png)

#Preguntas

* ¿Nuestro cliente GNU/Linux NFS puede acceder al servidor Windows NFS? Comprobarlo

Si puede

Para empezar, comprobamos la conectividad desde el cliente al servidor:

![pregunta1-01](./images/pregunta1-01.png)

Probamos a montar el recurso compartido desde el cliente:

![pregunta1-02](./images/pregunta1-02.png)

Comprobamos el acceso a los recursos NFS desde el cliente:

![pregunta1-03](./images/pregunta1-03.png)

Probé a crear ficheros y directorios en `/mnt/remoto08/public`:

![pregunta1-04](./images/pregunta1-04.png)

Y un directorio en `/mnt/remoto08/private`:

![pregunta1-05](./images/pregunta1-05.png)

El resultado es que se pueden observar los ficheros/carpetas desde el servidor en la carpeta `public`, pero en la `private` no:

![pregunta1-06](./images/pregunta1-06.png)

* ¿Nuestro cliente Windows NFS podría acceder al servidor GNU/Linux NFS? Comprobarlo

Si, si se puede

Para empezar, consultamos desde nuestro cliente Windows los recursos que ofrece el servidor OpenSUSE NFS:

![01aa](./images/01aa.png)

A continuación, montamos el recurso remoto:

![02aa](./images/02aa.png)

Comprobamos en el cliente Windows los recursos montados:

![03aa](./images/03aa.png)

A continuación, comprobamos el acceso a los recursos NFS desde el cliente Windows:

![04aa](./images/04aa.png)

![05aa](./images/05aa.png)

Intenté crear un directorio, pero no me lo permitió:

![06aa](./images/06aa.png)

Y por último, comprobamos los recursos compartidos:

![07aa](./images/07aa.png)

* Fijarse en los valores de usuarios propietario y grupo propietario de los ficheros que se guardan en el servidor NFS, cuando los creamos desde una conexión cliente NFS.

Hacemos las comprobaciones pertinentes:

![01bb](./images/01bb.png)

![02bb](./images/02bb.png)

Ahora montamos cada recurso en su directorio local correspondiente, y comprobar los recursos montados en nuestras carpetas locales:

![03bb](./images/03bb.png)

Comprobamos acceso a los recursos NFS desde el cliente:

![04bb](./images/04bb.png)

Cree un directorio `/mnt/remoto08/public/compruebaesto` y un fichero `comprueba`:

![05bb](./images/05bb.png)

Aquí tenemos el propietario del directorio:

![06bb](./images/06bb.png)




