#A2: Acceso remoto VNC

Usaremos 4 MVs para esta actividad:

* Windows 7
* Windows 2012 Server
* OpenSUSE 13.2

#Conexiones remotas con VNC

Vamos a realizar las siguientes conexiones remotas VNC:

1. Acceder a Windows Server - desde Windows 7
1. Acceder a Windows Server - desde GNU/Linux OpenSUSE 13.2
1. Acceder a GNU/Linux OpenSUSE 13.2 - desde GNU/Linux OpenSUSE 13.2 (A lo mejor no hay que instalar el software cliente VNC)
1. Acceder a GNU/Linux OpenSUSE 13.2 - desde Windows 7

##Comprobaciones

* Ejecutar `netstat -ntap` en las MVs GNU/Linux
* Ejecutar `netstat -n` en las MVs Windows

#Instalación

##Instalación en Windows

TightVNC es una herramienta libre disponible para Windows. En el servidor VNC usaremos TightVNC server y en el cliente usaremos TightVNC viewer

A continuación muestro una captura de la configuración de red de la MV Windows 2012 Server:

![red-2012](./images/red-2012.png)

Ejecuté los siguientes comandos desde la consola:

![comando1-2012](./images/comando1-2012.png)

![comando2-2012](./images/comando2-2012.png)

![comando3-2012](./images/comando3-2012.png)

![comando4-2012](./images/comando4-2012.png)

* A continuación comenzamos con la instalación del TightVNC en el Windows Server:

![vnc01-2012](./images/vnc01-2012.png)

Aceptamos los términos de la licencia y le damos a Next:

![vnc02-2012](./images/vnc02-2012.png)

Elegimos la opción Typical:

![vnc03-2012](./images/vnc03-2012.png)

Dejamos todo por defecto y le damos a Next:

![vnc04-2012](./images/vnc04-2012.png)

Especificamos las contraseñas y le damos a OK:

![vnc05-2012](./images/vnc05-2012.png)
![vnc06-2012](./images/vnc06-2012.png)

Ya hemos finalizado la instalación. Hacemos click en Finish:

![vnc07-2012](./images/vnc07-2012.png)

Configuración de red de W7:

![red-w7](./images/red-w7.png)

A continuación capturas de la ejecución de los comandos:

![comando1](./images/comando1.png)

![comando2](./images/comando2.png)

![comando3](./images/comando3.png)

![comando4](./images/comando4.png)

![comando5](./images/comando5.png)

* Ahora procederemos con la instalación del mismo programa en la MV Windows 7:

También elegimos la Typical:

![vnc01-client](./images/vnc01-client.png)

Aquí tenemos que desmarcar las dos casillas del medio:

![vnc02-client](./images/vnc02-client.png)

Y le damos a Finish:

![vnc03-client](./images/vnc03-client.png)

* Ahora vamos a conectarnos a la MV Windows Server desde W7:

Abrimos el programa, ponemos la IP de la MV 2012 Server y hacemos click en Connect:

![vnc04-client](./images/vnc04-client.png)

Introducimos la contraseña y le damos a OK:

![vnc05-client](./images/vnc05-client.png)

Ahora desde la MV 2012 Server, ejecuté el siguiente comando para verificar que se había establecido la conexión remota:

![netstat01](./images/netstat01.png)

##Instalación en OpenSUSE

En OpenSUSE se puede instalar/activar el servidor VNC directamente desde Yast -> VNC 
vncviewer es un cliente VNC que viene en OpenSUSE
En la conexión remota, hay que especificar IP:5901
vncviewer IP-vnc-server:5901

A continuación le dejo las capturas de los comandos que había que ejecutar:

![oscomando01](./images/oscomando01.png)

![oscomando02](./images/oscomando02.png)

Elegimos los siguientes ajustes:

![01](./images/01.png)

Ejecutamos el comando `vncviewer 172.18.8.21` para iniciar la conexión:

![02](./images/02.png)

Ejecutamos el comando para probar que la conexión es exitosa:

![netstat02](./images/netstat02.png)

> Hasta ahora hemos hecho dos de las cuatro conexiones. A continuación procedo con las dos siguientes

* Acceder a OpenSUSE 13.2 desde OpenSUSE 13.2:

Desde una de las dos MV abrí el programa VNC e introduje la IP de la MV a la que me quería conectar y el puerto:

![0X](./images/0X.png)

Luego hice el comando de comprobación: 

![0Y](./images/0Y.png)

> Tuve problema con la conexión de OpenSUSE 13.2 a OpenSUSE 13.2

* Acceder a OpenSUSE 13.2 desde Windows 7

Una vez en W7, abrir el TightVNC e introducir la IP de la MV de OpenSUSE 13.2 junto con el puerto:

![0Z](./images/0Z.png)

Hice el comando de comprobación, aunque esta conexión tampoco me salió exitosa:

![0A](./images/0A.png)

A continuación muestro una imagen para que se vea que se intentó y poder ver el error que me salía:

![0B](./images/0B.png)
























