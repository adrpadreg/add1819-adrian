#Servidor LDAP - OpenSUSE

#Servidor LDAP

##Preparar la máquina

* Vamos a usar una MV OpenSUSE 13.2 para montar nuestro servidor LDAP con:

-Nombre equipo: `ldap-server08`

-Además en `/etc/hosts` añadiremos:

![01](./images/01.png)

> Como siempre, configuraremos el acceso remoto para que el profesor pueda conectarse vía SSH a nuestras dos MV OpenSUSE

##Instalación del Servidor LDAP

* Procedemos a la instalación del módulo Yast que sirve para gestionar el servidor LDAP:

![02](./images/02.png)

* Ir a Yast -> `Authentication Server`

* Nos pedirá que instalemos los paquetes openldap2, krb5-server y krb5-client.

![03](./images/03.png)

![04](./images/04.png)

![05](./images/05.png)

![06](./images/06.png)

![07](./images/07.png)

Aqui tenemos un resumen de la configuración final:

![08](./images/08.png)

Ahora hacemos las siguientes comprobaciones:

* `systemctl status slapd`, para comprobar el estado del servicio:

![09](./images/09.png)

* `nmap localhost | grep -P '389|636'`, para comprobar que el servidor LDAP es accesible desde la red y `slapcat`, para comprobar que la base de datos está bién configurada:

![10](./images/10.png)

![11](./images/11.png)

* Podemos comprobar el contenido de la base de datos LDAP usando la herramienta `gq`. Esta herramienta es un browser LDAP. Comprobar que tenemos creadas las unidades organizativas: `groups` y `people`

![12](./images/12.png)

## Crear usuarios y grupos LDAP

* `Yast -> Usuarios Grupos -> Filtro -> LDAP`

* Crear el grupo `piratas` y los usuarios `pirata21` y `pirata22`

![13](./images/13.png)

#Autenticación

##Preparativos

* Cliente LDAP con OpenSUSE 13.2:

-Nombre equipo: `ldap-client08`

-Dominio: `curso1617`

-Asegurarse que tenemos definido en el fichero /etc/hosts del cliente, el nombre DNS con su IP correspondiente.

Comprobación

* `nmap ldap-server08 | grep -P '389|636'`, para comprobar que el servidor LDAP es accesible desde el cliente:

![14](./images/14.png)

* Usar `gq` en el cliente para comprobar que se han creado bien los usuarios

-`File -> Preferencias -> Servidor -> Nuevo`

-URl = `ldap://ldap-server08`

-Base DN = `dc=adrian08,dc=curso1617`

![15](./images/15.png)

##Instalar cliente LDAP

* Debemos instalar el paquete `yast2-auth-client`, que nos ayudará a configurar la máquina para autenticación. En Yast aparecerá como `Authentication Client`

![16](./images/16.png)

* Vamos a `Yast -> Authentication client`

![17](./images/17.png)

* Le damos a `Aceptar` y vamos a la consola para hacer algunas comprobaciones:

![18](./images/18.png)

Y por último, dejo una captura de `gq` desde el cliente:

![19](./images/19.png)











