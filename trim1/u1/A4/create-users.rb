#!/usr/bin/ruby

if ARGV.size != 1 then
	puts "Usar el programa <create-users> con 1 argumento"
	puts " filename : Nombre de fichero"
	exit 1
end

user = `whoami`

if user != "root\n" then
	puts "[INFO] Para poder ejecutar este script tienes que ser superusuario!"
	exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines = content.split("\n")

lines.each do |i|
	puts "Creando a: #{i}"
	system("useradd -d /home/#{i} -m #{i}")
	system("mkdir /home/#{i}/private")
	system("chmod 700 /home/#{i}/private")
	system("mkdir /home/#{i}/group")
	system("chmod 750 /home/#{i}/group")
	system("mkdir /home/#{i}/public")
	system("chmod 755 /home/#{i}/public")		
end
