#Acceso remoto SSH

Vamos a necesitar 3 MVs:

* Un servidor GNU/Linux OpenSUSE (IP 172.18.8.31)
* Un cliente GNU/Linux OpenSUSE (IP 172.18.8.32)
* Un cliente Windows7 (IP 172.18.8.11)

##Configuración de red de las MVs:

* Servidor GNU/Linux OpenSUSE (ssh-server08)

![redOS1](./images2/redOS1.png)

Había que añadir en `/etc/hosts` los equipos `ssh-client08a` y `ssh-client08b`

Para comprobar los cambios, teníamos que ejecutar los siguientes comandos:

![comandos01](./images2/comandos01.png)

![comandos02](./images2/comandos02.png)

![comandos03](./images2/comandos03.png)

![comandos04](./images2/comandos04.png)

![comandos05](./images2/comandos05.png)

Luego tuve que crear los usuarios `padron1`, `padron2`, `padron3` y `padron4` en la MV Server:

![users-server](./images2/users-server.png)

* Cliente GNU/Linux (ssh-client08a)

![redOS2](./images2/redOS2.png)

Aquí también tenemos que añadir en `/etc/hosts` los equipos `ssh-server08` y `ssh-client08b`

Comprobar haciendo ping a ambos equipos:

![pingOSclient](./images2/pingOSclient.png)

* Cliente Windows (ssh-client08b)

![redW](./images2/redW.png)

Teníamos que instalar software cliente SSH en Windows (PuTTY)

Aquí también había que añadir los equipos en `C:\Windows\System32\drivers\etc\hosts`

Comprobar haciendo ping a ambos equipos:

![ping-W7](./images2/ping-W7.png)

> Había que configurar el firewall de las MVs. En Windows para permitir que funcionen los ping y en OpenSUSE lo haremos con Yast, lo dejamos activo pero permitiendo el acceso externo únicamente a los servicios que necesitamos (Ir a `Servicios autorizados` -> `Zona externa`)

##Instalación del servicio SSH

Comprobación:

![ssh-server-comp](./images2/ssh-server-comp.png)

Comprobar que el servicio está escuchando por el puerto 22 con `netstat -ntap`:

![netstat](./images2/netstat.png)

Primera conexión SSH desde ssh-client08a:

* Comprobamos la conectividad con el servidor desde el cliente:

![prim01](./images2/prim01.png)

* Desde el cliente comprobamos que el servicio SSH es visible con `nmap ssh-server08`. Debe mostrarnos que el puerto 22 está abierto:

![prim02](./images2/prim02.png)

* Vamos a comprobar el funcionamiento de la conexión SSH desde cada cliente usando el usuario `padron1`. Desde `ssh-client08a` nos conectamos mediante `ssh padron1@ssh-server08`:

![prim03](./images2/prim03.png)

* Si nos volvemos a conectar tendremos:

![prim04](./images2/prim04.png)

* Comprobar el contenido del fichero `$HOME/.ssh/known_hosts` en el equipo ssh-client08a (es la clave de identificación de la máquina ssh-server08):

![prim05](./images2/prim05.png)

* Comprobar las conexiones desde los dos clientes:

Primero hice ping a la MV Server:

![prim06](./images2/prim06.png)

Abrimos PuTTY y nos sale esta "alerta de seguridad":

![prim07](./images2/prim07.png)

Y entramos con "padron1" desde el cliente W7:

![prim08](./images2/prim08.png)

¿Y si cambiamos las claves del servidor?

* Confirmar que existen los siguientes ficheros en `/etc/ssh`. Los ficheros `ssh_host*key` y `ssh_host*key.pub`, son ficheros de clave pública/privada que identifican a nuestro servidor frente a nuestros clientes:

![seg01](./images2/seg01.png)

* Modificar el fichero de configuración SSH (`/etc/ssh/sshd_config`) para dejar una única línea: `HostKey /etc/ssh/ssh_host_rsa_key`. Comentar el resto de líneas con configuración HostKey. Este parámetro define los ficheros de clave pública/privada que van a identificar a nuestro servidor. Con este cambio decimos que sólo vamos a usar las claves del tipo RSA

![seg02](./images2/seg02.png)

Regenerar certificados. Vamos a cambiar o volver a generar nuevas claves públicas/privadas para la identificación de nuestro servidor

* En "ssh-server08", como usuario root ejecutamos `ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key`. No poner password al certificado de la máquina:

![seg03](./images2/seg03.png)

* Reiniciar el servicio SSH:

![seg04](./images2/seg04.png)

* Comprobar que el servicio está en ejecución correctamente:

![seg05](./images2/seg05.png)

Comprobamos

* Comprobar qué sucede al volver a conectarnos desde los dos clientes, usando los usuarios `padron2` y `padron1`

![padron1-2L](./images2/padron1-2L.png)

Hay que volver a generar nuevas claves públicas/privadas:

![seg06](./images2/seg06.png)

Y ahora ya nos deja conectarnos:

![seg07](./images2/seg07.png)

Ahora comprobamos desde Windows:

![padron1W](./images2/padron1W.png)

![padron2W](./images2/padron2W.png)

##Personalización del prompt Bash

* Por ejemplo, podemos añadir las siguientes líneas al fichero de configuración del usuario "padron1" en la máquina `ssh-server08` (fichero `/home/padron1/.bashrc`)

![seg08](./images2/seg08.png)

* Además, crear el fichero `home/padron1/.alias` con el siguiente contenido:

![alias](./images2/alias.png)

* Comprobar funcionamiento de la conexión SSH desde cada cliente:

Comprobación desde OpenSUSE:

![compr-L](./images2/compr-L.png)

Comprobación desde Windows:

![compr-W1](./images2/compr-W1.png)

![compr-W2](./images2/compr-W2.png)

##Autenticación mediante claves públicas

El objetivo de este apartado es el de configurar SSH para poder acceder desde el ssh-client08a, usando "padron4" sin poner password, pero usando claves pública/privada. Para ello, vamos a configurar la autenticación mediante clave pública para acceder con nuestro usuario personal desde el equipo cliente al servidor con el usuario `padron4`

* Vamos a la máquina ssh-client08a
* No usar el usuario root

* Iniciamos sesión con nuestro usuario `adrian` de la máquina ssh-client08a
* Ejecutamos `ssh-keygen -t rsa` para generar un nuevo par de claves para el usuario:

![seg09](./images2/seg09.png)

* Ahora vamos a copiar la clave pública (id_rsa.pub) del usuario "adrian" de la máquina cliente, al fichero "authorized_keys" del usuario remoto "padron4". Usando el comando `ssh-copy-id`:

![segXX](./images2/segXX.png)

* Comprobar que ahora al acceder remotamente vía SSH:

Desde ssh-client08a, NO se pide password

![segXY](./images2/segXY.png)

Desde ssh-client08b, SI se pide password

![segXZ](./images2/segXZ.png)

##Uso de SSH como túnel para X

* Instalar en el servidor una aplicación de entorno gráfico (Geany) que no esté en los clientes:

![5-01](./images2/5-01.png)

* Modificar servidor SSH para permitir la ejecución de aplicaciones gráficas, desde los clientes. COnsultar fichero de configuración `/etc/ssh/sshd_config`:

![5-02](./images2/5-02.png)

* Comprobar funcionamiento de Geany desde ssh-client08a. Con el comando `ssh -X padron1@ssh-server08` podemos conectarnos de forma remota al servidor, y ahora ejecutamos Geany de forma remota:

![5-03](./images2/5-03.png)

##Aplicaciones Windows nativas

Podemos tener aplicaciones Windows nativas instaladas en ssh-server mediante el emulador WINE

* Instalar emulador Wine en el ssh-server

![seis-01](./images2/seis-01.png)

* Podemos usar el Block de Notas que viene con Wine: wine notepad. Comprobar el funcionamiento del Block de Notas que viene con Wine, accediendo desde "ssh-client08a":

![seis-02](./images2/seis-02.png)

> En este caso hemos conseguido implementar una solución similar a RemoteApps usando SSH

##Restricciones de uso

Vamos a modificar los usuarios del servidor SSH para añadir algunas restricciones de uso del servicio.

1. Sin restricción (tipo 1)

Usuario sin restricciones:

* El usuario `padron1` podrá conectarse vía SSH sin restricciones
* En principio no es necesario tocar nada

Comprobación OpenSUSE:

![restriction01-1](./images2/restriction01-1.png)

Comprobación Windows:

![restriction01-2](./images2/restriction01-2.png)

1. Restricción total (tipo 2)

Vamos a crear una restricción de uso del SSH para un usuario:

* En el servidor tenemos el usuario padron2. Desde local en el servidor podemos usar sin problemas el usuario
* Vamos a modificar SSH de modo que al usar el usuario por ssh desde los clientes tendremos permiso denegado

* Consultar/modificar fichero de configuración del servidor SSH (`/etc/ssh/sshd_config`) para restringir el acceso a determinados usuarios.

![restriction2-01](./images2/restriction2-01.png)

* Comprobar la restricción al acceder desde los clientes:

![restriction2-02-1](./images2/restriction2-02-1.png)

![restriction2-02-2](./images2/restriction2-02-2.png)

1. Restricción sobre aplicaciones (tipo 4)

Vamos a crear una restricción de permisos sobre determinadas aplicaciones.

* Usaremos el usuario `padron4`
* Crear grupo remoteapps. Incluir al usuario en el grupo

![restriction4-01](./images2/restriction4-01.png)

![restriction4-02](./images2/restriction4-02.png)

* Localizar el programa Geany. Poner al programa el grupo propietario a remoteapps:

![restriction4-03](./images2/restriction4-03.png)

* Poner los permisos del ejecutable de Geany a 750. Para impedir que los que no pertenezcan al grupo puedan ejecutar el programa:

![restriction4-04](./images2/restriction4-04.png)

* Comprobamos el funcionamiento desde los clientes (OpenSUSE y Windows):

![restriction4-05](./images2/restriction4-05.png)

![restriction4-06](./images2/restriction4-06.png)















