#!/usr/bin/ruby

if ARGV.size != 1 then
	puts "Usar el programa <install-and-remove> con 1 argumento"
	puts " filename : Nombre de fichero"
	exit 1
end

user = `whoami`

if user != "root\n" then
	puts "[INFO] Hay que ser el usuario root!"
	exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines = content.split("\n")

lines.each do |row|
	items = row.split(':')
	check = `whereis #{items[0]} |grep bin |wc -l`
	check2 = check.to_i
	if items[1] == "status" or items[1] == "s" then
		if check2 == 0 then
			puts "El paquete #{items[0]} NO está instalado"
		elsif check2 != 0 then
			puts "El paquete #{items[0]} ya está instalado"
		end
	elsif items[1] == "install" or items[1] == "i" then
		if check2 == 0 then
			system("zypper install #{items[0]}")
		end
	elsif items[1] == "remove" or items[1] == "r" then
		if check2 != 0 then
			system("zypper remove #{items[0]}")
		end
	end
end
	 
	
